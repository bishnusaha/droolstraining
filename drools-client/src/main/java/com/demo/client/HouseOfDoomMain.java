package com.demo.client;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.demo.model.Location;

public class HouseOfDoomMain {

    public static void main(String[] args) throws Exception {
    	
    	KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
        KieSession ksession = kContainer.newKieSession();

        ksession.insert( new Location("Office", "House") );
        ksession.insert( new Location("Kitchen", "House") );
        ksession.insert( new Location("Knife", "Kitchen") );
        ksession.insert( new Location("Cheese", "Kitchen") );
        ksession.insert( new Location("Desk", "Office") );
        ksession.insert( new Location("Chair", "Office") );
        ksession.insert( new Location("Computer", "Desk") );
        ksession.insert( new Location("Drawer", "Desk") );

        ksession.insert( "go1" );
        ksession.fireAllRules();
        System.out.println("---");

        ksession.insert( "go2" );
        ksession.fireAllRules();
        System.out.println("---");

        ksession.insert( "go3" );
        ksession.fireAllRules();
        System.out.println("---");

        ksession.insert( new Location("Key", "Drawer") );
        ksession.fireAllRules();
        System.out.println("---");

        ksession.insert( "go4" );
        ksession.fireAllRules();
        System.out.println("---");

        ksession.insert( "go5" );
        ksession.fireAllRules();
    }
}
