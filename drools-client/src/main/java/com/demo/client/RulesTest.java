package com.demo.client;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.KieServices;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

import com.demo.model.Application;
import com.demo.model.Student;

public class RulesTest {

  //private static final String containerName = "Demo_1.0.0-SNAPSHOT";
  private static final String containerName = "firstPoc_1.0.0-SNAPSHOT";
  //private static final String sessionName = "myStatelessSession";

  public static final void main(String[] args) {
    try {
      // Define KIE services configuration and client:
      Set<Class<?>> allClasses = new HashSet<Class<?>>();
      allClasses.add(Student.class);
      String serverUrl = "http://localhost:8080/kie-server/services/rest/server";
      String username = "kieserver";
      String password = "kieserver1!";
      KieServicesConfiguration config =
        KieServicesFactory.newRestConfiguration(serverUrl,
                                                username,
                                                password);
      config.setMarshallingFormat(MarshallingFormat.JSON);
      config.addExtraClasses(allClasses);
      config.setTimeout(50000);
      KieServicesClient kieServicesClient =
        KieServicesFactory.newKieServicesClient(config);

      // Set up the fact model:
      Student s = new Student();
      s.setName("Sam");
      s.setMarks(82.50);
      
      Application application = new Application();
      application.setDateApplied(new Date("2020/03/02"));

      // Insert Person into the session:
      KieCommands kieCommands = KieServices.Factory.get().getCommands();
      List<Command> commandList = new ArrayList<Command>();
      //commandList.add(kieCommands.newInsert(s, "studentObj"));
      commandList.add(kieCommands.newInsert(application,"application"));

      // Fire all rules:
      commandList.add(kieCommands.newFireAllRules("numberOfFiredRules"));
      BatchExecutionCommand batch = kieCommands.newBatchExecution(commandList);

      // Use rule services client to send request:
      RuleServicesClient ruleClient = kieServicesClient.getServicesClient(RuleServicesClient.class);
      ServiceResponse<ExecutionResults> executeResponse = ruleClient.executeCommandsWithResults(containerName, batch);
      System.out.println("number of fired rules:" + executeResponse.getResult().getValue("numberOfFiredRules"));
      //System.out.println("Student Grade After Rule:" + ((Student)executeResponse.getResult().getValue("studentObj")).getGrade());
      System.out.println("Student Grade After Rule:" + ((Application)executeResponse.getResult().getValue("application")));
    }

    catch (Throwable t) {
      t.printStackTrace();
    }
  }
}
