package com.demo.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.command.Command;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.command.CommandFactory;

import com.demo.model.Applicant;
import com.demo.model.Application;
import com.demo.model.Fire;
import com.demo.model.Order;
import com.demo.model.Person;
import com.demo.model.Room;
import com.demo.model.Sprinkler;
import com.demo.model.Student;

public class RulesMavenTest {
	
	public void executeRuleUsingSingleFact() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		StatelessKieSession kSession = kContainer.newStatelessKieSession();
		Applicant applicant = new Applicant();
		applicant.setName("Mr John Smith");
		applicant.setAge(16);
		applicant.setValid(true);
		
		System.out.println("Before Executing the rule applicant :: "+applicant);
		
		kSession.execute(applicant);

		System.out.println("After Executing the rule applicant :: "+applicant);		
	}
	
	public void executeRuleUsingMultiFact() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		StatelessKieSession kSession = kContainer.newStatelessKieSession();
		Applicant applicant = new Applicant();
		applicant.setName("Mr John Smith");
		applicant.setAge(16);
		
		Application application = new Application();
		application.setDateApplied(new Date(System.currentTimeMillis()));
		application.setValid(true);
		
		System.out.println("Before Executing the rule application :: "+application);
		
		kSession.execute(new Object[]{applicant,application});

		System.out.println("After Executing the rule application :: "+application);	
	}
	
	public void executeRuleUsingCommand() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		StatelessKieSession kSession = kContainer.newStatelessKieSession();
		
		Person p1 = new Person();
		p1.setName("Mr John Smith");
		p1.setAge(102);
		
		Person p2 = new Person();
		p2.setName("Mr John Doe");
		p2.setAge(16);
		
		List<Command> cmds = new ArrayList<Command>();		
		cmds.add(CommandFactory.newInsert(p1, "mrSmith"));
		cmds.add(CommandFactory.newInsert(p2, "mrDoe"));
		
		System.out.println("Before Executing the rule mrSmith :: "+p1);	
		System.out.println("Before Executing the rule mrDoe :: "+p2);	
		
		ExecutionResults results = kSession.execute(CommandFactory.newBatchExecution(cmds));
		
		System.out.println("After Executing the rule mrSmith :: "+results.getValue("mrSmith"));	
		System.out.println("After Executing the rule mrDoe :: "+results.getValue("mrDoe"));
				
	}
	
	public void executeRuleUsingstatefulksession() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		KieSession ksession = kContainer.newKieSession();
		
		String[] names = new String[]{"kitchen", "bedroom", "office", "livingroom"};
		Map<String,Room> name2room = new HashMap<String,Room>();
		for( String name: names ){
		    Room room = new Room( name );
		    name2room.put( name, room );
		    ksession.insert( room );
		    Sprinkler sprinkler = new Sprinkler( room );
		    ksession.insert( sprinkler );
		}

		ksession.fireAllRules();
		
		Fire kitchenFire = new Fire( name2room.get( "kitchen" ) );
		Fire officeFire = new Fire( name2room.get( "office" ) );

		FactHandle kitchenFireHandle = ksession.insert( kitchenFire );
		FactHandle officeFireHandle = ksession.insert( officeFire );

		ksession.fireAllRules();
		
		ksession.delete( kitchenFireHandle );
		ksession.delete( officeFireHandle );

		ksession.fireAllRules();
		ksession.dispose();
	}
	
	public void testOrderDtable() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.demospace", "firstPoc", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		KieSession ksession = kContainer.newKieSession();
		ksession.addEventListener( new DefaultAgendaEventListener() {
			   public void afterMatchFired(AfterMatchFiredEvent event) {
			       super.afterMatchFired( event );
			       System.out.println( event );
			   }
			});
		
		Order o = new Order();
		o.setItemsCount(2);
		//o.setDeliverInDays(1);
		
		ksession.insert(o);
		
		ksession.fireAllRules();
		
		
		ksession.dispose();
	}
	
public void testInference() {
		
		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.myspace", "Demo", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);
		
		KieSession ksession = kContainer.newKieSession();
		ksession.addEventListener( new DefaultAgendaEventListener() {
			   public void afterMatchFired(AfterMatchFiredEvent event) {
			       super.afterMatchFired( event );
			       System.out.println( event );
			   }
			});
		
		System.out.println(ksession.getFactCount());
		
		Person p = new Person();
		p.setName("Mr John Doe");
		p.setAge(16);
		
		FactHandle personFactHandle = ksession.insert(p);
		
		ksession.fireAllRules();
		
		p.setAge(20);
		ksession.update(personFactHandle, p);
		
		ksession.fireAllRules();
		
		ksession.dispose();
	}
	
	public static void main(String[] args) {
		
		RulesMavenTest rulesMavenTest = new RulesMavenTest();
		//rulesMavenTest.executeRuleUsingSingleFact();
		//rulesMavenTest.executeRuleUsingMultiFact();
		//rulesMavenTest.executeRuleUsingCommand();
		//rulesMavenTest.executeRuleUsingstatefulksession();
		//rulesMavenTest.testInference();
		rulesMavenTest.testOrderDtable();
	}

}
