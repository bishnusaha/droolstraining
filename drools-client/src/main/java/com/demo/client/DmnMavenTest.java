package com.demo.client;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieRuntimeFactory;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;

public class DmnMavenTest {

	public static void main(String[] args) {

		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId("com.demospace", "firstPoc", "1.0.0-SNAPSHOT");
		KieContainer kContainer = kieServices.newKieContainer(releaseId);

		DMNRuntime dmnRuntime = KieRuntimeFactory.of(kContainer.getKieBase()).get(DMNRuntime.class);

		String modelNamespace = "https://kiegroup.org/dmn/_3001D5DA-0B24-4F2B-8129-7B3EB6412344";
		String modelName = "insurance-premium";

		DMNModel dmnModel = dmnRuntime.getModel(modelNamespace, modelName);

		DMNContext dmnContext = dmnRuntime.newContext();
		dmnContext.set("Age", 16);
		dmnContext.set("Had Previous Incident", true);

		DMNResult dmnResult = dmnRuntime.evaluateAll(dmnModel,dmnContext);

		for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
			System.out.println("Insurance Premium: " + dr.getResult());
		}

	}

}
