package com.demo.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNResult;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.DMNServicesClient;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;

public class DmnClientTest {

	public static void main(String[] args) {
		
		//executeInsurancePremium();
		executeFlightBooking();

	}

	private static void executeInsurancePremium() {
		String kieContainerId = "firstPoc_1.0.0-SNAPSHOT";
		String modelNamespace = "https://kiegroup.org/dmn/_3001D5DA-0B24-4F2B-8129-7B3EB6412344";
		String modelName = "insurance-premium";
		
		DMNServicesClient dmnClient = getDmnClient();

		DMNContext dmnContext = dmnClient.newContext();
		dmnContext.set("Age", 30);
		dmnContext.set("Had Previous Incident", true);

		ServiceResponse<DMNResult> serverResp = dmnClient.evaluateAll(kieContainerId, modelNamespace, modelName, dmnContext);

		DMNResult dmnResult = serverResp.getResult();
		for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
			System.out.println("Insurance Premium: " + dr.getResult());
		}
	}
	
	private static void executeFlightBooking() {
		String kieContainerId = "firstPoc_1.0.0-SNAPSHOT";
		String modelNamespace = "https://kiegroup.org/dmn/_D1B91F00-5C05-44A5-86DE-5BEF2A37E104";
		String modelName = "Flight-rebooking";
		
		DMNServicesClient dmnClient = getDmnClient();
		
		List<Map<String, Object>> fligtList = new ArrayList<Map<String,Object>>();
		
		Map<String, Object> flight1= new HashMap<String, Object>();
		flight1.put("Flight Number", "123");
		flight1.put("Status", "cancelled");
		flight1.put("From", "TX");
		flight1.put("To", "NY");
		flight1.put("Departure", "2020-02-29T23:59:00");
		flight1.put("Capacity", 1);
		
		Map<String, Object> flight2= new HashMap<String, Object>();
		flight2.put("Flight Number", "124");
		flight2.put("Status", "cancelled");
		flight2.put("From", "TX");
		flight2.put("To", "NY");
		flight2.put("Departure", "2020-02-28T23:59:00");
		flight2.put("Capacity", 1);
		
		Map<String, Object> flight3= new HashMap<String, Object>();
		flight3.put("Flight Number", "125");
		flight3.put("Status", "scheduled");
		flight3.put("From", "TX");
		flight3.put("To", "NY");
		flight3.put("Departure", "2020-03-01T00:59:00");
		flight3.put("Capacity", 3);
		
		fligtList.add(flight1);
		fligtList.add(flight2);
		fligtList.add(flight3);
		
		List<Map<String, Object>> passengerList = new ArrayList<Map<String,Object>>();
		
		Map<String, Object> passenger1= new HashMap<String, Object>();
		passenger1.put("Flight Number", "123");
		passenger1.put("Status", "gold");
		passenger1.put("Miles", 50);
		passenger1.put("Name", "Jhon");
		
		Map<String, Object> passenger2= new HashMap<String, Object>();
		passenger2.put("Flight Number", "123");
		passenger2.put("Status", "silver");
		passenger2.put("Miles", 80);
		passenger2.put("Name", "Jake");
		
		Map<String, Object> passenger3= new HashMap<String, Object>();
		passenger3.put("Flight Number", "124");
		passenger3.put("Status", "gold");
		passenger3.put("Miles", 80);
		passenger3.put("Name", "Sam");
		
		passengerList.add(passenger1);
		passengerList.add(passenger2);
		passengerList.add(passenger3);

		DMNContext dmnContext = dmnClient.newContext();
		dmnContext.set("Flight List", fligtList);
		dmnContext.set("Passenger List", passengerList);

		ServiceResponse<DMNResult> serverResp = dmnClient.evaluateAll(kieContainerId, modelNamespace, modelName, dmnContext);

		DMNResult dmnResult = serverResp.getResult();
		for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
			System.out.println("Flight booking passenger priority List: " + dr.getResult());
		}
	}
	
	public static DMNServicesClient getDmnClient()
	{
		String serverUrl = "http://localhost:8080/kie-server/services/rest/server";
		String username = "kieserver";
		String password = "kieserver1!";
		

		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(serverUrl, username, password);
		config.setMarshallingFormat(MarshallingFormat.JSON);
		config.setTimeout(50000);
		KieServicesClient kieServicesClient = KieServicesFactory.newKieServicesClient(config);

		DMNServicesClient dmnClient = kieServicesClient.getServicesClient(DMNServicesClient.class);
		
		return dmnClient;
	}

}
