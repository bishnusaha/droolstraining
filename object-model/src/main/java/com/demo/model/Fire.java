package com.demo.model;

import java.io.Serializable;

public class Fire implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Room room;	

	public Fire(Room room) {
		super();
		this.room = room;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "Fire [room=" + room + "]";
	}

}
