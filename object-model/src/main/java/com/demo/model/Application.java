package com.demo.model;

import java.io.Serializable;
import java.util.Date;

public class Application implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date dateApplied;
	private boolean valid;
	
	public Date getDateApplied() {
		return dateApplied;
	}
	public void setDateApplied(Date dateApplied) {
		this.dateApplied = dateApplied;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return "Application [dateApplied=" + dateApplied + ", valid=" + valid + "]";
	}
}
