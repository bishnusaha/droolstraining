package com.demo.model;

import java.io.Serializable;

public class Room implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;	

	public Room(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Room [name=" + name + "]";
	}
}
