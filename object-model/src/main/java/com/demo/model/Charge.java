package com.demo.model;

import java.io.Serializable;

public class Charge implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private double amount;

	public Charge(double amount) {
		super();
		this.amount = amount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Charge [amount=" + amount + "]";
	}
}
