package com.demo.model;

import java.io.Serializable;

public class Order implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int itemsCount;
	private Integer deliverInDays;
	private double totalPrice;
	
	public int getItemsCount() {
		return itemsCount;
	}
	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}
	public Integer getDeliverInDays() {
		return deliverInDays;
	}
	public void setDeliverInDays(Integer deliverInDays) {
		this.deliverInDays = deliverInDays;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Override
	public String toString() {
		return "Order [itemsCount=" + itemsCount + ", deliverInDays=" + deliverInDays + ", totalPrice=" + totalPrice
				+ "]";
	}

}
