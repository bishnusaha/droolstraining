package com.demo.model;

import java.io.Serializable;

public class ChildBusPass implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Person person;
	
	public ChildBusPass(Person person) {
		super();
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "ChildBusPass [person=" + person + "]";
	}

}
