package com.demo.model;

import java.io.Serializable;

public class AdultBusPass implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Person person;
	
	public AdultBusPass(Person person) {
		super();
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "AdultBusPass [person=" + person + "]";
	}

}
